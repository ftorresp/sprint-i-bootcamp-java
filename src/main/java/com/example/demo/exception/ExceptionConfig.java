package com.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.ExceptionDTO;

@ControllerAdvice(annotations = RestController.class)
public class ExceptionConfig {
  
  @ExceptionHandler(UserNotFoundException.class)
  public ResponseEntity<ExceptionDTO> userNotFoundHandler(Exception e) {
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ExceptionDTO(e.getMessage(), e.getClass().getName(), 404));
  }
  
  @ExceptionHandler(NotInstanceOfSellerException.class)
  public ResponseEntity<ExceptionDTO> notInstanceOfSellerHandler(Exception e) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ExceptionDTO(e.getMessage(), e.getClass().getName(), 400));
  }
  
  @ExceptionHandler(FollowAlreadyExistsException.class)
  public ResponseEntity<ExceptionDTO> followAlreadyExistsHandler(Exception e) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ExceptionDTO(e.getMessage(), e.getClass().getName(), 400));
  }

  @ExceptionHandler(FollowNotFoundException.class)
  public ResponseEntity<ExceptionDTO> followNotFoundHandler(Exception e) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ExceptionDTO(e.getMessage(), e.getClass().getName(), 400));
  }

}
