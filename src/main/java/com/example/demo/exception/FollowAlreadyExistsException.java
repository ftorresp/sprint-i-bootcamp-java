package com.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "El usuario ya sigue al vendedor")
public class FollowAlreadyExistsException extends RuntimeException {
  
  public FollowAlreadyExistsException(String message) {
    super(message);
  }

}
