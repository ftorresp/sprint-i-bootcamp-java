package com.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "El usuario a seguir no es un vendedor.")
public class NotInstanceOfSellerException extends RuntimeException {

  public NotInstanceOfSellerException(String message) {
    super(message);
  }
  
}
