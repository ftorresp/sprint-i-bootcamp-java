package com.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "El follow no existe")
public class FollowNotFoundException extends RuntimeException {
  
  public FollowNotFoundException(String message) {
    super(message);
  }

}
