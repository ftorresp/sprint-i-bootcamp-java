package com.example.demo.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.example.demo.entity.Buyer;
import com.example.demo.entity.Seller;
import com.example.demo.entity.User;

@Repository
public class UserRepository {
  
  private List<User> usersList = new ArrayList<>();
  private Integer id = 0;

  // public UserRepository() {
  //   init();
  // }

  public void storeUser(User user) {
    user.setId(generateId());
    usersList.add(user);
  }

  public User getUserById(Integer id) {
    Optional<User> user = usersList
                          .stream()
                          .filter(u -> u.getId() == id)
                          .findFirst();
    return user.orElse(null);
  }

  public List<User> getUsersByIdsList(List<Integer> idsList) {
    return usersList
            .stream()
            .filter(u -> {
              Optional<Integer> user = idsList
                                        .stream()
                                        .filter(i -> i == u.getId())
                                        .findFirst();
              return user.isPresent();
            })
            .collect(Collectors.toList());
  }

  // puee

  public Integer generateId() {
    id += 1;
    return id;
  }

}
