package com.example.demo.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.example.demo.entity.Publication;

@Repository
public class PublicationRepository {
  
  private List<Publication> publicationsList = new ArrayList<>();
  private Integer id = 0;

  public void storePublication(Publication publication) {
    publication.setId(generateId());
    publicationsList.add(publication);
  }

  public List<Publication> getPublicationsBySellerId(Integer id) {
    return publicationsList
            .stream()
            .filter(p -> p.getUser_id() == id)
            .collect(Collectors.toList());
  }

  public Integer getPromoPublicationsCount(Integer id) {
    List<Publication> filteredPublications = publicationsList
                                                .stream()
                                                .filter(p -> p.getUser_id() == id && p.getHas_promo())
                                                .collect(Collectors.toList());
    return filteredPublications.size();
  }

  public List<Publication> getPromoPublicationsList(Integer id) {
    return publicationsList
            .stream()
            .filter(p -> p.getUser_id() == id && p.getHas_promo())
            .collect(Collectors.toList());
  }

  public Integer generateId() {
    this.id += 1;
    return this.id;
  }

}
