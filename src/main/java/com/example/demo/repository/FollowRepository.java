package com.example.demo.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.example.demo.entity.Follow;

@Repository
public class FollowRepository {
  
  List<Follow> followsList = new ArrayList<>();

  public void storeFollow(Follow follow) {
    followsList.add(follow);
  }

  public Follow getFollow(Integer followerId, Integer followedId) {
    Optional<Follow> follow = followsList
            .stream()
            .filter(f -> f.getFollower_id() == followerId && f.getFollowed_id() == followedId)
            .findFirst();

    return follow.orElse(null);
  }

  public List<Integer> getFollowersIdsByUserId (Integer id) {
    return followsList
            .stream()
            .filter(f -> f.getFollowed_id() == id)
            .map(f -> f.getFollower_id())
            .collect(Collectors.toList());
  }

  public List<Integer> getFollowedIdsByUserId (Integer id) {
    return followsList
            .stream()
            .filter(f -> f.getFollower_id() == id )
            .map(f -> f.getFollowed_id())
            .collect(Collectors.toList());
  }

  public void destroyFollow(Integer followerId, Integer followedId) {
    Follow follow = getFollow(followerId, followedId);
    if (follow != null) {
      followsList.remove(follow);
    }
  }

}
