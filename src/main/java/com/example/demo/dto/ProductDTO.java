package com.example.demo.dto;

import lombok.Data;

@Data
public class ProductDTO {
  
  private Integer product_id;
  private String product_name;
  private String type;
  private String brand;
  private String color;
  private String notes;

}
