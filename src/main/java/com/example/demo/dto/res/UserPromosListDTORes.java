package com.example.demo.dto.res;

import java.util.List;

import com.example.demo.entity.Publication;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserPromosListDTORes {

  private Integer user_id;
  private String user_name;
  private List<Publication> posts;
  
}
