package com.example.demo.dto.res;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserFollowersCountDTORes {
  
  private Integer user_id;
  private String user_name;
  private Integer followers_count;

}
