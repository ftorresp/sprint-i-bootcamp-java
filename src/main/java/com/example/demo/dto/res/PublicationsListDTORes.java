package com.example.demo.dto.res;

import java.util.List;

import com.example.demo.entity.Publication;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PublicationsListDTORes {
  
  private Integer user_id;
  private List<Publication> posts;

}
