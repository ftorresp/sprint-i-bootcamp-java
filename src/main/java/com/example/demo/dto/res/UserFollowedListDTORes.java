package com.example.demo.dto.res;

import java.util.List;

import com.example.demo.dto.UserDTO;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserFollowedListDTORes {
  
  private Integer user_id;
  private String user_name;
  private List<UserDTO> followed;

}
