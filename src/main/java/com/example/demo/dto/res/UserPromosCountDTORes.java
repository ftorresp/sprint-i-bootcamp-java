package com.example.demo.dto.res;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserPromosCountDTORes {
  
  private Integer userId;
  private String user_name;
  private Integer promo_products_count;

}
