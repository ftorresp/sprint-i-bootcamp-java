package com.example.demo.dto.req;

import java.time.LocalDate;

import com.example.demo.dto.ProductDTO;

import lombok.Data;

@Data
public class PublicationDTOReq {
  
  private Integer user_id;
  private LocalDate date;
  private ProductDTO product;
  private String category;
  private Double price;

}
