package com.example.demo.dto;

import com.example.demo.entity.User;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserDTO {
  
  private Integer user_id;
  private String user_name;

  public UserDTO(User user) {
    user_id = user.getId();
    user_name = user.getName();
  }

}
