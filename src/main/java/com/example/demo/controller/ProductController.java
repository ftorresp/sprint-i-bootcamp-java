package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.req.PromoPublicationDTOReq;
import com.example.demo.dto.req.PublicationDTOReq;
import com.example.demo.dto.res.PublicationsListDTORes;
import com.example.demo.dto.res.UserPromosCountDTORes;
import com.example.demo.dto.res.UserPromosListDTORes;
import com.example.demo.service.IPublicationService;

@RestController
@RequestMapping("products")
public class ProductController {
  
  @Autowired
  IPublicationService publicationService;

  @PostMapping("/post")
  public ResponseEntity<?> storePublication(@RequestBody PublicationDTOReq publicationDTO) {
    publicationService.storePublication(publicationDTO);
    return ResponseEntity.ok().build();
  }

  @PostMapping("/promo-post")
  public ResponseEntity<?> storePromoPublication(@RequestBody PromoPublicationDTOReq PublicationDTO) {
    publicationService.storePromoPublication(PublicationDTO);
    return ResponseEntity.ok().build();
  }

  @GetMapping("/followed/{userId}/list")
  public ResponseEntity<PublicationsListDTORes> getPublicationsList(
      @PathVariable Integer userId,
      @RequestParam(required = false) String order
    ) {
    

    PublicationsListDTORes publicationsListDTO = publicationService.getPublicationsByBuyerId(userId);

    if (order != null) {
      publicationsListDTO.setPosts(publicationService.sortByDate(publicationsListDTO.getPosts(), order));
    }
    return ResponseEntity.ok(publicationsListDTO);
  }

  @GetMapping("/promo-post/count")
  public ResponseEntity<UserPromosCountDTORes> getPromoPublicationsCount(@RequestParam Integer user_id) {
    return ResponseEntity.ok(publicationService.getPromoPublicationsCount(user_id));
  }

  @GetMapping("/promo-post/list")
  public ResponseEntity<UserPromosListDTORes> getPromoPublicationsList(@RequestParam Integer user_id) {
    return ResponseEntity.ok(publicationService.getPromoPublicationsList(user_id));
  }

}
