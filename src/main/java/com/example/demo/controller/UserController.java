package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.res.UserFollowedListDTORes;
import com.example.demo.dto.res.UserFollowersCountDTORes;
import com.example.demo.dto.res.UserFollowersListDTORes;
import com.example.demo.service.IUserService;

@RestController
@RequestMapping("users")
public class UserController {

  @Autowired
  IUserService userService;

  @PostMapping("/{userId}/follow/{userIdToFollow}")
  public ResponseEntity<?> storeUserFollow(@PathVariable Integer userId, @PathVariable Integer userIdToFollow) {
    userService.storeFollow(userId, userIdToFollow);
    return ResponseEntity.ok().build();
  }

  @PostMapping("/{userId}/unfollow/{userIdToUnfollow}")
  public ResponseEntity<?> destroyUserFollow(@PathVariable Integer userId, @PathVariable Integer userIdToUnfollow) {
    userService.destroyFollow(userId, userIdToUnfollow);
    return ResponseEntity.ok().build();
  }

  @GetMapping("/{userId}/followers/count")
  public ResponseEntity<UserFollowersCountDTORes> getFollowersCount(@PathVariable Integer userId) {
    UserFollowersCountDTORes userFollowersCountDTO = userService.getFollowersCount(userId);
    return ResponseEntity.ok(userFollowersCountDTO);
  }

  @GetMapping("/{userId}/followers/list")
  public ResponseEntity<UserFollowersListDTORes> getFollowersList(
      @PathVariable Integer userId,
      @RequestParam(required = false) String order
    ) {
    
    UserFollowersListDTORes userFollowersListDTO = userService.getFollowersList(userId);
    if (order != null) {
      userFollowersListDTO.setFollowers(userService.sortByName(userFollowersListDTO.getFollowers(), order));
    }
    return ResponseEntity.ok(userFollowersListDTO);
  }

  @GetMapping("/{userId}/followed/list")
  public ResponseEntity<UserFollowedListDTORes> getFollowedList(
      @PathVariable Integer userId,
      @RequestParam(required = false) String order
    ) {
    UserFollowedListDTORes userFollowedListDTO = userService.getFollowedList(userId);
    if (order != null) {
      userFollowedListDTO.setFollowed(userService.sortByName(userFollowedListDTO.getFollowed(), order));
    }
    return ResponseEntity.ok(userFollowedListDTO);
  }
  
}
