package com.example.demo.controller;

import java.lang.reflect.Method;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.ExceptionDTO;

@RestController
public class ErrorController {
  
  @RequestMapping(value = "/error", method = {
    RequestMethod.GET,
    RequestMethod.POST,
    RequestMethod.PUT,
    RequestMethod.PATCH,
    RequestMethod.DELETE
  })
  // @GetMapping("/error")
  public ResponseEntity<?> error() {
    return ResponseEntity.status(404).body(new ExceptionDTO("Path not found", null, 404));
  }

}
