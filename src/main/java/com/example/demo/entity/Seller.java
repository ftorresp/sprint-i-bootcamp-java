package com.example.demo.entity;

public class Seller extends User {

  public Seller(String name) {
    super.setName(name);
    super.setRole("seller");
  }
  
  @Override
  public void followSeller(Seller seller) {};

  public void storePublication(Publication publication) {};

}
