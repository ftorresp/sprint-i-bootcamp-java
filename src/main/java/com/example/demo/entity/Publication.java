package com.example.demo.entity;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Publication {
  
  private Integer id;
  private Integer user_id;
  private LocalDate date;
  private Product product;
  private String category;
  private Double price;  
  private Boolean has_promo = false;
  private Double discount;

}
