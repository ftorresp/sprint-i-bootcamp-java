package com.example.demo.entity;

public class Buyer extends User {

  public Buyer(String name) {
    super.setName(name);
    super.setRole("buyer");
  }
  
  @Override
  public void followSeller(Seller seller) {};

}
