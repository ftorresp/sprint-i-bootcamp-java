package com.example.demo.entity;

import lombok.Data;

@Data
public class Follow {
  
  private Integer followed_id;
  private Integer follower_id;

  public Follow (Integer followerId, Integer followedId) {
    follower_id = followerId;
    followed_id = followedId;
  }

}
