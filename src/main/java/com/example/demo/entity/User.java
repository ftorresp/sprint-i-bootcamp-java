package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class User {
  
  private Integer id;
  private String name;
  private String role;

  public abstract void followSeller(Seller seller);

}
