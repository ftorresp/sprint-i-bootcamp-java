package com.example.demo.config;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.example.demo.entity.Buyer;
import com.example.demo.entity.Follow;
import com.example.demo.entity.Product;
import com.example.demo.entity.Publication;
import com.example.demo.entity.Seller;
import com.example.demo.repository.FollowRepository;
import com.example.demo.repository.PublicationRepository;
import com.example.demo.repository.UserRepository;

import jakarta.annotation.PostConstruct;

@Profile("dev")
@Configuration
public class DevConfig {

  @Autowired
  UserRepository userRepository;

  @Autowired
  FollowRepository followRepository;

  @Autowired
  PublicationRepository publicationRepository;
  
  @PostConstruct
  public void initializer() {
    userRepository.storeUser(new Buyer("Hugo"));
    userRepository.storeUser(new Buyer("Paco"));
    userRepository.storeUser(new Buyer("Luis"));

    userRepository.storeUser(new Seller("Juan"));
    userRepository.storeUser(new Seller("Pedro"));

    followRepository.storeFollow(new Follow(1, 4));
    followRepository.storeFollow(new Follow(2, 4));
    followRepository.storeFollow(new Follow(3, 4));
    followRepository.storeFollow(new Follow(1, 5));

    Product product = new Product(1, "Silla Gamer", "Gamer", "Racer", "Red & Black", "Special Edition");
    publicationRepository.storePublication(
      new Publication(
        null, 
        4, 
        LocalDate.of(2022, 12, 19), 
        product, 
        "100", 
        1500.50, 
        false, 
        null
      )
    );
    publicationRepository.storePublication(
      new Publication(
        null, 
        5, 
        LocalDate.of(2022, 12, 19), 
        product, 
        "100", 
        1500.50, 
        false, 
        null
      )
    );

  }

}
