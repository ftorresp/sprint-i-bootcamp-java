package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.UserDTO;
import com.example.demo.dto.res.UserFollowedListDTORes;
import com.example.demo.dto.res.UserFollowersCountDTORes;
import com.example.demo.dto.res.UserFollowersListDTORes;
import com.example.demo.entity.User;

public interface IUserService {
  
  public void storeFollow(Integer userId, Integer userIdToFollow);

  public UserFollowersCountDTORes getFollowersCount(Integer userId);

  public UserFollowersListDTORes getFollowersList(Integer userId);

  public UserFollowedListDTORes getFollowedList(Integer userId);

  public List<UserDTO> sortByName(List<UserDTO> list, String order);

  public void destroyFollow(Integer userId, Integer userIdToUnfollow);

  public User getUserById(Integer userId);

}
