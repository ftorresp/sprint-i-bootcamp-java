package com.example.demo.service;

import java.util.List;

public interface IFollowService {
  
  public void storeFollow(Integer followerId, Integer followedId);

  public Integer getUserFollowersCount(Integer userId);

  public List<Integer> getFollowersById(Integer userId);

  public List<Integer> getFollowedById(Integer userId);

  public void destroyFollow(Integer followerId, Integer followedId);

}
