package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.req.PromoPublicationDTOReq;
import com.example.demo.dto.req.PublicationDTOReq;
import com.example.demo.dto.res.PublicationsListDTORes;
import com.example.demo.dto.res.UserPromosCountDTORes;
import com.example.demo.dto.res.UserPromosListDTORes;
import com.example.demo.entity.Publication;

public interface IPublicationService {
  
  public void storePublication(PublicationDTOReq publicationDTO);


  public void storePromoPublication(PromoPublicationDTOReq publicationDTO);


  public PublicationsListDTORes getPublicationsByBuyerId(Integer userId);


  public UserPromosCountDTORes getPromoPublicationsCount(Integer userId);
  

  public UserPromosListDTORes getPromoPublicationsList(Integer userId);

  public List<Publication> sortByDate(List<Publication> list, String order);

}
