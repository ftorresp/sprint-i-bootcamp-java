package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Follow;
import com.example.demo.exception.FollowAlreadyExistsException;
import com.example.demo.exception.FollowNotFoundException;
import com.example.demo.repository.FollowRepository;

@Service
public class FollowService implements IFollowService {
  
  @Autowired
  FollowRepository followRepository;

  public void storeFollow(Integer followerId, Integer followedId) {
    if (followRepository.getFollow(followerId, followedId) != null) {
      throw new FollowAlreadyExistsException("El usuario con id  " + followerId + " ya sigue al usuario con id " + followedId);
    }

    followRepository.storeFollow(new Follow(followerId, followedId));
  }

  public Integer getUserFollowersCount(Integer userId) {
    return followRepository.getFollowersIdsByUserId(userId).size();
  }

  public List<Integer> getFollowersById(Integer userId) {
    return followRepository.getFollowersIdsByUserId(userId);
  }

  public List<Integer> getFollowedById(Integer userId) {
    return followRepository.getFollowedIdsByUserId(userId);
  }

  public void destroyFollow(Integer followerId, Integer followedId) {
    if (followRepository.getFollow(followerId, followedId) == null) {
      throw new FollowNotFoundException("El usuario " + followerId + " no sigue al usuario " + followedId);
    }

    followRepository.destroyFollow(followerId, followedId);
  }

}
