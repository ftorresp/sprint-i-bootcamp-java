package com.example.demo.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.example.demo.dto.UserDTO;
import com.example.demo.dto.res.UserFollowedListDTORes;
import com.example.demo.dto.res.UserFollowersCountDTORes;
import com.example.demo.dto.res.UserFollowersListDTORes;
import com.example.demo.entity.Seller;
import com.example.demo.entity.User;
import com.example.demo.exception.NotInstanceOfSellerException;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.repository.UserRepository;

@Service
public class UserService implements IUserService {
  
  @Autowired
  UserRepository userRepository;

  @Autowired
  IFollowService followService;

  public void storeFollow(Integer userId, Integer userIdToFollow) {
    
    if (userRepository.getUserById(userId) == null) {
      throw new UserNotFoundException("No existe el usuario con id " + userId);
    }

    if (userRepository.getUserById(userIdToFollow) == null) {
      throw new UserNotFoundException("No existe el usuario con id " + userIdToFollow);
    }

    if (!(userRepository.getUserById(userIdToFollow) instanceof Seller)) {
      throw new NotInstanceOfSellerException("El usuario con id: " + userIdToFollow + " no es un vendedor");
    }

    followService.storeFollow(userId, userIdToFollow);
  }

  public void destroyFollow(Integer userId, Integer userIdToUnfollow) {
    followService.destroyFollow(userId, userIdToUnfollow);
  }

  public User getUserById(Integer userId) {
    return userRepository.getUserById(userId);
  }

  public UserFollowersCountDTORes getFollowersCount(Integer userId) {
    User user = userRepository.getUserById(userId);

    if (user == null) {
      throw new UserNotFoundException("No existe el usuario con id " + userId);
    }

    if (!(user instanceof Seller)) {
      throw new NotInstanceOfSellerException("El usuario con id: " + userId + " no es un vendedor");
    }

    Integer followersCount = followService.getUserFollowersCount(user.getId());
    return new UserFollowersCountDTORes(user.getId(), user.getName(), followersCount);
  }

  public UserFollowersListDTORes getFollowersList(Integer userId) {
    User user = userRepository.getUserById(userId);

    if (user == null) {
      throw new UserNotFoundException("No existe el usuario con id " + userId);
    }

    if (!(user instanceof Seller)) {
      throw new NotInstanceOfSellerException("El usuario con id: " + userId + " no es un vendedor");
    }

    List<Integer> followersIds = followService.getFollowersById(userId);
    List<User> users = userRepository.getUsersByIdsList(followersIds);
    List<UserDTO> usersDTO = users.stream().map(u -> new UserDTO(u)).collect(Collectors.toList());
    return new UserFollowersListDTORes(userId, user.getName(), usersDTO);
  }

  public UserFollowedListDTORes getFollowedList(Integer userId) {
    User user = userRepository.getUserById(userId);

    if (user == null) {
      throw new UserNotFoundException("No existe el usuario con id " + userId);
    }
    
    List<Integer> followedIds = followService.getFollowedById(userId);
    List<User> users = userRepository.getUsersByIdsList(followedIds);
    List<UserDTO> usersDTO = users.stream().map(u -> new UserDTO(u)).collect(Collectors.toList());
    return new UserFollowedListDTORes(userId, user.getName(), usersDTO);
  }

  public List<UserDTO> sortByName(List<UserDTO> list, String order) {
    
    List<UserDTO> sortedList = list;

    if (order.equals("name_asc")) {
      Collections.sort(sortedList, Comparator.comparing(UserDTO::getUser_name));
    }

    if (order.equals("name_desc")) {
      Collections.sort(sortedList, Comparator.comparing(UserDTO::getUser_name));
      Collections.reverse(sortedList);
    }
    
    return sortedList;
  }

}
