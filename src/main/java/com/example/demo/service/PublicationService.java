package com.example.demo.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.req.PromoPublicationDTOReq;
import com.example.demo.dto.req.PublicationDTOReq;
import com.example.demo.dto.res.PublicationsListDTORes;
import com.example.demo.dto.res.UserPromosCountDTORes;
import com.example.demo.dto.res.UserPromosListDTORes;
import com.example.demo.entity.Publication;
import com.example.demo.entity.Seller;
import com.example.demo.entity.User;
import com.example.demo.exception.NotInstanceOfSellerException;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.repository.PublicationRepository;

@Service
public class PublicationService implements IPublicationService {
  
  @Autowired
  PublicationRepository publicationRepository;

  @Autowired
  IFollowService followService;

  @Autowired
  IUserService userService;

  ModelMapper mapper = new ModelMapper();

  public void storePublication(PublicationDTOReq publicationDTO) {
    Integer userId = publicationDTO.getUser_id();
    if (userService.getUserById(userId) == null) {
      throw new UserNotFoundException("El usuario con id " + userId + " no existe");
    }

    if (!(userService.getUserById(userId) instanceof Seller)) {
      throw new NotInstanceOfSellerException("El usuario no puede hacer publicaciones");
    }
    
    Publication publication = mapper.map(publicationDTO, Publication.class);
    publicationRepository.storePublication(publication);
  }


  public void storePromoPublication(PromoPublicationDTOReq publicationDTO) {
    Integer userId = publicationDTO.getUser_id();
    if (userService.getUserById(userId) == null) {
      throw new UserNotFoundException("El usuario con id " + userId + " no existe");
    }

    if (!(userService.getUserById(userId) instanceof Seller)) {
      throw new NotInstanceOfSellerException("El usuario no puede hacer publicaciones");
    }

    Publication publication = mapper.map(publicationDTO, Publication.class);
    publicationRepository.storePublication(publication);
  }


  public PublicationsListDTORes getPublicationsByBuyerId(Integer userId) {
    if (userService.getUserById(userId) == null) {
      throw new UserNotFoundException("El usuario con id " + userId + " no existe");
    }
    
    List<Integer> followedIds = followService.getFollowedById(userId);
    List<Publication> publicationsList = new ArrayList<>();
    for (Integer id : followedIds) {
      publicationsList.addAll(publicationRepository.getPublicationsBySellerId(id));
    }

    List<Publication> filteredList = publicationsList
                                      .stream()
                                      .filter(p -> p.getDate().isAfter(LocalDate.now().minusWeeks(2)))
                                      .sorted(Comparator.comparing(Publication::getDate))
                                      .collect(Collectors.toList());

    // Collections.sort(publicationsList, Comparator.comparing(Publication::getDate));
    
    return new PublicationsListDTORes(userId, filteredList);
  }


  public UserPromosCountDTORes getPromoPublicationsCount(Integer userId) {
    User user = userService.getUserById(userId);
    if (userService.getUserById(userId) == null) {
      throw new UserNotFoundException("El usuario con id " + userId + " no existe");
    }

    Integer count = publicationRepository.getPromoPublicationsCount(userId);
    return new UserPromosCountDTORes(userId, user.getName(), count);
  }
  

  public UserPromosListDTORes getPromoPublicationsList(Integer userId) {
    User user = userService.getUserById(userId);
    if (userService.getUserById(userId) == null) {
      throw new UserNotFoundException("El usuario con id " + userId + " no existe");
    }

    List<Publication> publications = publicationRepository.getPromoPublicationsList(userId);
    return new UserPromosListDTORes(userId, user.getName(), publications);
  }

  public List<Publication> sortByDate(List<Publication> list, String order) {

    if (order.equals("date_asc")) {
      Collections.sort(list, Comparator.comparing(Publication::getDate));
    }

    if (order.equals("date_desc")) {
      Collections.sort(list, Comparator.comparing(Publication::getDate));
      Collections.reverse(list);
    }

    return list;
  }

}
